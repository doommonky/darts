﻿using System;
using System.Collections.Generic;
using System.Linq;
using Darts.Core;
using Darts.Games.Types;
using Newtonsoft.Json;

namespace Darts.Games.Scores
{
    public class CricketScore : GameScoreBase
    {
        private readonly int[] _cricketValues = new int[7] { 20, 19, 18, 17, 16, 15, 25 };
        private readonly int[] _lowballValues = new int[7] { 1, 2, 3, 4, 5, 6, 25 };
        private readonly CricketType _type;

        public CricketScore(CricketType type)
        {
            _type = type;
            Score = 0;
            Marks = new Dictionary<int, int>();

            int[] list;
            switch (type)
            {
                case CricketType.Cricket:
                    list = _cricketValues;
                    break;
                case CricketType.Lowball:
                    list = _lowballValues;
                    break;
                default:
                    throw new InvalidOperationException();
            }
            foreach (var i in list)
            {
                Marks.Add(i, 0);
            }
        }

        public override IGameScore Clone()
        {
            return JsonConvert.DeserializeObject<CricketScore>(JsonConvert.SerializeObject(this));
        }

        public Dictionary<int, int> Marks { get; private set; }

        //returns true only if this player is the last player to hit three marks.
        public bool ScoreThrow(Throw @throw, bool valueLocked = false)
        {
            //returns early if the throw isn't possible.
            if (!@throw.Valid)
                return false;

            //returns early if the value hit isn't a relevant cricket value.
            if (_type == CricketType.Cricket && !_cricketValues.Contains(@throw.ValueHit) ||
                _type == CricketType.Lowball && !_lowballValues.Contains(@throw.ValueHit))
                return false;

            var hits = @throw.Multiplier;
            //Score marks while there are still marks to score.
            while (Marks[@throw.ValueHit] < 3 && hits > 0)
            {
                Marks[@throw.ValueHit]++;
                hits--;
            }
            //If value is closed and there are still hits to score and it hasn't been locked for scoring, add it up!
            if (hits > 0 && !valueLocked)
            {
                Score += @throw.ValueHit * hits;
            }

            //Return true if this player is the last to close the value.
            //This value is now locked for scoring for everyone.
            if (valueLocked && Marks[@throw.ValueHit] == 3)
                return true;

            return false;
        }
    }
}
