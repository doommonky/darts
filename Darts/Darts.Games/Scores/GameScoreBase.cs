﻿using Darts.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Darts.Games.Scores
{
    public class GameScoreBase : IGameScore
    {
        public Player Player { get; set; }
        public int Score { get; set; }

        public virtual IGameScore Clone()
        {
            return JsonConvert.DeserializeObject<GameScoreBase>(JsonConvert.SerializeObject(this));
        }
    }
}
