﻿using System;
using System.Collections.Generic;
using System.Linq;
using Darts.Core;
using Darts.Games.Scores;
using Darts.Games.Options;

namespace Darts.Games
{
    public class X01 : IGame
    {
        //TODO: X01 still needs Double In, Double Out options.
        public int InitialScore { get; private set; }
        private Dictionary<X01OptionType, bool> _options = new Dictionary<X01OptionType, bool>();
        private readonly Dictionary<string, GameScoreBase> _playerScores = new Dictionary<string, GameScoreBase>();
        private readonly List<Throw> _turn = new List<Throw>();
        private bool _bust;
        private GameEngine _engine;

        public X01(int initialScore)
        {
            InitialScore = initialScore;
        }

        public void Initialize(GameEngine engine)
        {
            _engine = engine;
            foreach (var p in engine.Players)
            {
                _playerScores.Add(p.Name, new GameScoreBase());
                _playerScores[p.Name].Player = p;
                _playerScores[p.Name].Score = InitialScore;
            }

            //Add configuration ability.
            _options.Add(X01OptionType.DoubleIn, false);
            _options.Add(X01OptionType.DoubleOut, false);
        }

        public Tuple<bool, string> RecordThrow(Throw @throw)
        {
            //What dartboard are you using?!
            if (!@throw.Valid)
                return new Tuple<bool, string>(false, "That throw is not valid.");

            //Check for Double-Out before setting throw
            var throwScore = _playerScores[_engine.CurrentPlayer.Name].Score - _turn.Select(x => x.ThrowValue).Sum() - @throw.ThrowValue;
            if (_options[X01OptionType.DoubleOut] == true &&
                throwScore == 0 &&
                @throw.Multiplier != 2)
            {
                //TODO: This now requires more values on the return than just bust to pass messages out of the game.
                return new Tuple<bool, string>(false, "One must hit a double to win the game.");
            }

            _turn.Add(@throw);

            //Check for bust.
            if (_playerScores[_engine.CurrentPlayer.Name].Score < _turn.Select(x => x.ThrowValue).Sum())
            {
                _bust = true;
                return new Tuple<bool, string> (true, $"{_engine.CurrentPlayer.Name} has busted.");
            }

            return new Tuple<bool, string>(false, "Current Score: " + (_playerScores[_engine.CurrentPlayer.Name].Score - _turn.Select(x => x.ThrowValue).Sum()));
        }

        public bool CheckVictory(Player player)
        {
            if (_playerScores[player.Name].Score - _turn.Select(x => x.ThrowValue).Sum() == 0)
            {
                return true;
            }
            return false;
        }
        
        public void StartTurn(Player player)
        {
            _bust = false;
            _turn.Clear();
        }

        public void EndTurn()
        {
            if (!_bust)
            {
                var sum = _turn.Select(x => x.ThrowValue).Sum();
                _playerScores[_engine.CurrentPlayer.Name].Score -= sum;
            }
        }

        public IEnumerable<IGameScore> GetGameState()
        {
            if (_bust)
                return _playerScores.Values.ToList();

            var result = new List<GameScoreBase>();
            foreach (var score in _playerScores.Values)
            {
                result.Add((GameScoreBase)score.Clone());
            }

            var playerScore = result.Where(x => x.Player.Name == _engine.CurrentPlayer.Name).First();
            foreach(var @throw in _turn)
            {
                playerScore.Score -= @throw.ThrowValue;
            }

            return result;
        }
    }
}
