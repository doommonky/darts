﻿using System;
using System.Collections.Generic;
using System.Linq;
using Darts.Core;
using Darts.Games.Scores;
using Darts.Games.Types;

namespace Darts.Games
{
    public class Cricket : IGame
    {
        private GameEngine _engine;
        private CricketType _type;
        private Dictionary<string, CricketScore> _playerScores = new Dictionary<string, CricketScore>();
        private List<int> _valuesLocked = new List<int>();

        public Cricket(CricketType type)
        {
            _type = type;
        }

        public void Initialize(GameEngine engine)
        {
            _engine = engine;
            foreach (var player in engine.Players)
            {
                _playerScores.Add(player.Name, new CricketScore(_type));
                _playerScores[player.Name].Player = player;
                _playerScores[player.Name].Score = 0;
            }
        }

        public void SetOptions(IEnumerable<IGameOption> options)
        {
            //Cricket has no relevant options that I'm aware of.
        }

        public void StartTurn(Player player)
        {
            //Cricket has no relevant turn state to track.
        }

        public Tuple<bool, string> RecordThrow(Throw @throw)
        {
            if (!@throw.Valid)
                return new Tuple<bool, string>(false, "That throw is not valid.");

            //This is a lot to do in line, I know. 
            //Scores the throw and then sets the lock list if this throw is the last to hit three of the relevant mark.
            SetLock(@throw.ValueHit, _playerScores[_engine.CurrentPlayer.Name].ScoreThrow(@throw, IsLocked(@throw.ValueHit)));
            return new Tuple<bool, string>(false, "Throw recorded.");
        }

        public bool CheckVictory(Player player)
        {
            //If a player hasn't closed all marks, they can't have won.
            if (_playerScores[player.Name].Marks.Any(x => x.Value < 3))
                return false;

            //Compare current player score against the rest of the players. 
            //Given the marks check is already done, if their score isnt' the highest,
            //they haven't won yet.
            var playerScore = _playerScores[player.Name].Score;
            foreach (var opponentScore in _playerScores.Where(x => x.Key != player.Name).Select(y => y.Value.Score))
            {
                if (playerScore < opponentScore)
                {
                    return false;
                }
            }

            //If we're past the previous two checks, the game is won. 
            return true;
        }

        public void EndTurn()
        {
            //Cricket has no relevant turn state to track.
        }

        public IEnumerable<IGameScore> GetGameState()
        {
            return _playerScores.Values.ToList();
        }

        private void SetLock(int value, bool locked)
        {   
            if (locked)
            {
                _valuesLocked.Add(value);
            }
        }

        //Checks to see if a given mark is locked for scoring.
        private bool IsLocked (int value)
        {
            //If it's already in the list, just return out. Confirmed locked, because everyone has already closed it.
            if (_valuesLocked.Contains(value))
                return true;

            //Check if everyone ELSE has already closed this mark. If so, this player will not be able to score on this value.
            bool locked = false;
            try
            {
                locked = _playerScores.Where(x => x.Key != _engine.CurrentPlayer.Name).Select(y => y.Value.Marks[value]).All(z => z == 3);
            } 
            catch (KeyNotFoundException)
            {
                //It doesn't matter of the key isn't found. It just means the value hit is a value that the game doesn't care about.
                return false; 
            }

            return locked;
        }
    }
}