﻿using Darts.Core;

namespace Darts.Games.Options
{
    public class X01Option : IGameOption
    {
        public X01Option (X01OptionType option, bool active)
        {
            Option = option;
            OptionActive = active;
        }

        public X01OptionType Option { get; private set; }
        public bool OptionActive { get; private set; }
    }

    public enum X01OptionType
    {
        DoubleIn,
        DoubleOut
    }
}
