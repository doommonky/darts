﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Darts.Core;

namespace Darts
{
    class Program
    {
        static void Main(string[] args)
        {
            var gameEngine = SetupGame();

            foreach (var player in gameEngine.Players)
            {
                for (var x = 0; x <= 2; x++)
                {
                    Console.WriteLine("{0}, throw number {1}", player.Name, x+1);
                    var @throw = new Throw (int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
                }
            }
        }

        private static GameEngine SetupGame()
        {
            Console.WriteLine("What game are we playing?");
            var gameType = Console.ReadLine();
            Console.WriteLine("How many players do we have today?");
            var playerCount = int.Parse(Console.ReadLine());

            var playerNames = new List<string>();

            for (var x = 0; x < playerCount; x++)
            {
                Console.WriteLine("Who is player {0}?", x + 1);
                playerNames.Add(Console.ReadLine());
            }

            var gameEngine = new GameEngine(gameType, playerNames);
            return gameEngine;
        }
    }
}
