﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Darts.Core;
using Darts.Games;
using Darts.Games.Scores;
using Darts.Games.Types;

namespace Darts
{
    class Program
    {
        static void Main()
        {
            var gameEngine = SetupGame();

            while (true)
            {
                foreach (var player in gameEngine.Players)
                {
                    gameEngine.StartTurn(player);
                    for (var x = 0; x <= 2; x++)
                    {
                        var @throw = GetThrow(player, x + 1);
                        if (!@throw.Valid)
                        {
                            Console.WriteLine("That is an invalid throw");
                            x--;
                            continue;
                        }

                        var playersTurnEnded = gameEngine.RecordThrow(@throw);
                        if (playersTurnEnded.Item1)
                        {
                            Console.WriteLine(playersTurnEnded.Item2);
                            break;
                        }
                        Console.Write($"\nCurrent Scores:\n{DrawScores(gameEngine.GetGameState())}\n");

                        var victoryMet = gameEngine.CheckVictory(player);
                        if (victoryMet)
                        {
                            Console.WriteLine("{0} has won the game.", player.Name);
                            Console.Write($"\nFinal Scores:\n{DrawScores(gameEngine.GetGameState())}");
                            Console.ReadKey();
                            Console.WriteLine("\nWould you like to play again? Y/N");
                            if (Console.ReadLine().ToString().ToLower() == "y")
                                Main();

                            return;
                        }
                    }
                    Console.Write($"\n{gameEngine.CurrentPlayer.Name}'s turn is over.\nCurrent Scores:\n{DrawScores(gameEngine.GetGameState())}\n");
                    gameEngine.EndTurn();
                }
            }
        }

        private static Throw GetThrow(Player player, int throwNumber)
        {
            Console.WriteLine("{0}, throw number {1}", player.Name, throwNumber);
            var throwValue = int.Parse(Console.ReadLine());
            var throwMultiplier = int.Parse(Console.ReadLine());
            var @throw = new Throw(throwValue, throwMultiplier);
            return @throw;
        }

        private static GameEngine SetupGame()
        {
            Console.WriteLine("What game are we playing?");
            var gameType = Console.ReadLine();
            var game = CreateGame(gameType);
            Console.WriteLine("How many players do we have today?");
            var playerCount = int.Parse(Console.ReadLine());

            var playerNames = new List<string>();

            for (var x = 0; x < playerCount; x++)
            {
                Console.WriteLine("Who is player {0}?", x + 1);
                playerNames.Add(Console.ReadLine());
            }
            
            var gameEngine = new GameEngine(game, playerNames);
            return gameEngine;
        }

        private static IGame CreateGame(string gameName)
        {
            switch (gameName.ToLower())
            {
                case "301":
                    return new X01(301);
                case "501":
                    return new X01(501);
                case "701":
                    return new X01(701);
                case "1001":
                    return new X01(1001);
                case "cricket":
                    return new Cricket(CricketType.Cricket);
                case "lowball":
                    return new Cricket(CricketType.Lowball);
                //case "countup":
                //    return new Countup();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static string DrawScores(IEnumerable<IGameScore> scores)
        {
            var sb = new StringBuilder();

            if (scores.First().GetType() != typeof(CricketScore))
            {
                foreach (var score in scores)
                {
                    sb.AppendLine($"{score.Player.Name}: {score.Score}");
                }
            }
            else
            {
                foreach (CricketScore score in scores)
                {
                    sb.AppendLine($"{score.Player.Name}: ");
                    foreach (var mark in score.Marks)
                    {
                        sb.Append($"{mark.Key}: {TranslateCricketMarkToVisual(mark.Value)} | ");
                    }
                    sb.Append($"Score: {score.Score}");
                    sb.AppendLine("");
                }
            }

            return sb.ToString();
        }

        private static string TranslateCricketMarkToVisual(int marks)
        {
            switch(marks)
            {
                case 3:
                    return "0";
                case 2:
                    return "X";
                case 1:
                    return "/";
                case 0:
                default:
                    return " ";
            }
        }
    }
}
