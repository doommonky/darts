﻿using System;

namespace Darts.Core
{
    public interface IGameScore
    {
        Player Player { get; set; }
        int Score { get; set; }

       IGameScore Clone();
    }
}
