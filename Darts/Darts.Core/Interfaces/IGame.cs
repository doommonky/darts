﻿using System;
using System.Collections.Generic;

namespace Darts.Core
{
    public interface IGame
    {
        void Initialize(GameEngine engine);
        void StartTurn(Player player);
        Tuple <bool, string> RecordThrow(Throw @throw);
        bool CheckVictory(Player player);
        void EndTurn();
        IEnumerable<IGameScore> GetGameState();
    }
}
