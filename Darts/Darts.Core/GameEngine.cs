﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Darts.Core
{
    public class GameEngine
    {
        public Guid GameEngineId { get; private set; }
        public List<Player> Players { get; private set; }
        public IGame Game { get; private set; }
        public Player CurrentPlayer { get; private set; }

        public GameEngine(IGame game, List<string> playerNames)
        {
            if (game == null) throw new ArgumentNullException("game");
            if (playerNames == null) throw new ArgumentNullException("playerNames");
            if (playerNames.Count <= 0) throw new Exception("Game requires at least one player");

            Game = game;
            Players = playerNames.Select(p => new Player(p)).ToList();
            
            Game.Initialize(this);
        }

        //public void SetOptions(IEnumerable<IGameOption> options)
        //{
        //    Game.SetOptions(options);
        //}

        public Tuple<bool,string> RecordThrow(Throw @throw)
        {
            return Game.RecordThrow(@throw);     
        }

        public bool CheckVictory(Player player)
        {
            return Game.CheckVictory(player);
        }

        public void StartTurn(Player player)
        {
            if (CurrentPlayer != null)
            {
                throw new InvalidOperationException("StartTurn method called without ending previous turn");
            }
            CurrentPlayer = player;
            Game.StartTurn(player);
        }

        public void EndTurn()
        {
            Game.EndTurn();
            CurrentPlayer = null;
        }

        public IEnumerable<IGameScore> GetGameState()
        {
            return Game.GetGameState();
        }
    }
}
