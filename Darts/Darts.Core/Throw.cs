﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Darts.Core
{
    public class Throw
    {
        public int ValueHit { get; private set; }
        public int Multiplier { get; private set; }
        public int ThrowValue { get; private set; }
        public bool Valid { get; private set; }

        public Throw(int hit, int mul)
        {
            Valid = ValidateThrow(hit, mul);

            ValueHit = hit;
            Multiplier = mul;
            ThrowValue = hit * mul;
        }

        private bool ValidateThrow(int hit, int mul)
        {
            if ((Enumerable.Range(1, 20).Contains(hit) && Enumerable.Range(1, 3).Contains(mul)))
                return true;

            if (hit == 25 && Enumerable.Range(1, 2).Contains(mul))
                return true;

            if (hit == 0 && mul == 0)
                return true;

            return false;
        }
    }
}
