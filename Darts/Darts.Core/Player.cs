﻿using System;
using System.Collections.Generic;

namespace Darts.Core
{
    public class Player
    {
        public string Name { get; set; }

        public Player(string name)
        {
            Name = name;
        }
    }
}
