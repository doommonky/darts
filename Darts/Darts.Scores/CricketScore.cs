﻿using System;
using System.Collections.Generic;
using System.Linq;
using Darts.Core;
using Darts.Games;

namespace Darts.Scores
{
    public class CricketScore : GameScoreBase
    {
        private int[] _cricketValues = new int[7] { 20, 19, 18, 17, 16, 15, 25 };
        private int[] _lowballValues = new int[7] { 1, 2, 3, 4, 5, 6, 25 };
        private CricketType _type;

        public CricketScore(CricketType type)
        {
            _type = type;
            Score = 0;
            Marks = new Dictionary<int, int>();

            int[] list;
            switch (type)
            {
                case CricketType.Cricket:
                    list = _cricketValues;
                    break;
                case CricketType.Lowball:
                    list = _lowballValues;
                    break;
                default:
                    throw new InvalidOperationException();
            }
            foreach (var i in list)
            {
                Marks.Add(i, 0);
            }
        }

        public Dictionary<int, int> Marks { get; private set; }

        public void ScoreThrow(Throw @throw)
        {
            if (!@throw.Valid)
                return;

            if (_type == CricketType.Cricket && !_cricketValues.Contains(@throw.ValueHit) ||
                _type == CricketType.Lowball && !_lowballValues.Contains(@throw.ValueHit))
                return;

            var hits = @throw.Multiplier;
            while (Marks[@throw.ValueHit] < 3 && hits > 0)
            {
                Marks[@throw.ValueHit]++;
                hits--;
            }
            if (hits > 0)
            {
                Score += @throw.ValueHit * hits;
            }
        }
    }
}
