﻿using Darts.Core;

namespace Darts.Scores
{
    public class GameScoreBase : IGameScore
    {
        public Player Player { get; set; }
        public int Score { get; set; }
    }
}
